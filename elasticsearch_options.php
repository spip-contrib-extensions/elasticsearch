<?php
/**
 * Options au chargement du plugin Elasticsearch
 *
 * @plugin     Elasticsearch
 * @copyright  2016
 * @author     Guy Cesaro
 * @licence    GNU/GPL
 * @package    SPIP\Elasticsearch\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

defined('_ELASTICSEARCH_PAS_INDEXATION')   || define('_ELASTICSEARCH_PAS_INDEXATION', 500);