<?php
/**
 * Définit les autorisations du plugin Elasticsearch
 *
 * @plugin     Elasticsearch
 * @copyright  2016
 * @author     Guy Cesaro
 * @licence    GNU/GPL
 * @package    SPIP\Elasticsearch\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/*
 * Un fichier d'autorisations permet de regrouper
 * les fonctions d'autorisations de votre plugin
 */

/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function elasticsearch_autoriser(){}


/* Exemple
function autoriser_elasticsearch_configurer_dist($faire, $type, $id, $qui, $opt) {
	// type est un objet (la plupart du temps) ou une chose.
	// autoriser('configurer', '_elasticsearch') => $type = 'elasticsearch'
	// au choix :
	return autoriser('webmestre', $type, $id, $qui, $opt); // seulement les webmestres
	return autoriser('configurer', '', $id, $qui, $opt); // seulement les administrateurs complets
	return $qui['statut'] == '0minirezo'; // seulement les administrateurs (même les restreints)
	// ...
}
*/