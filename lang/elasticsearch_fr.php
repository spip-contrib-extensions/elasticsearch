<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(


	'elasticsearch_titre' => 'Elasticsearch',


	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_label_activer_elasticsearch_objets' => 'Activer l\'indexation pour les contenus :',
	'cfg_titre_parametrages' => 'Paramétrages',

	'aucun_resultat' => 'Pas de résultat',
	'nb_resultat' => '<span class="chiffre">1</span> résultat',
	'nb_resultats' => '<span class="chiffre">@nb@</span> résultats',
	'pour_votre_recherche' => 'pour votre recherche « @recherche@ »',
	'titre_page_configurer_elasticsearch' => 'Configuration d\'Elasticsearch',
	'cfg_explication_nom_index' => 'Nom sans espace ou caractères spéciaux',
	'cfg_label_nom_index' => 'Nom de l\'index',
	'cfg_index_encours' => 'Indexation en cours',
);